# README #

docker build -f Dockerfile -t zippa/admin-mongo:latest .


## Env variable Definition ##

* **HOST**	The IP address adminMongo will listen on
* **PORT**	The Port adminMongo will listen on
* **PASSWORD** An application level password to add simply authentication ( TO DO  )
* **CONN_NAME**	The name of the connection to create on boot
* **DB_USERNAME**	The username for the database connection
* **DB_PASSWORD**	The password for the database user
* **DB_HOST**	The host IP address or DNS name without the port!
* **DB_PORT**	The port of the mongoDB database, if not provided the default 27017 will be used
* **DB_NAME**	The name of the database


Default admin pwd : "secureadminpassword"

## Use custom app.json and config.json file 

docker run -d -v <alternative-config-dir>:/app/user/alternative -p 1234:1234 zippa/admin-mongo:latest


### New version related to commit a4c1908
