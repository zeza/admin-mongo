#!/bin/bash

set -e

ADMINMONGO_HOME=/app/user
ADMINMONGO_CONF=$ADMINMONGO_HOME/config
ADMINMONGO_ALTERVATIVE=$ADMINMONGO_HOME/alternative

if [ -f $ADMINMONGO_ALTERVATIVE/config.json ]
then
  # Move in custom configuration
  echo "=> Using custom config.json"
  cp $ADMINMONGO_ALTERVATIVE/config.json $ADMINMONGO_CONF/config.json
fi

if [ -f $ADMINMONGO_ALTERVATIVE/app.json ]
then
  # Move in custom configuration
  echo "=> Using custom app.json"
  cp $ADMINMONGO_ALTERVATIVE/app.json $ADMINMONGO_CONF/app.json
fi


# Start app
echo "=> Starting app on port $PORT..."
exec "$@"